package ua.hillel;

public enum Role {
    DIRECTOR,
    MANAGER,
    SALESMAN,
    INTERN;
}
